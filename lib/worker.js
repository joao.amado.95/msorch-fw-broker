const Zeebe = require('zeebe-node')
const { zeebe_broker, worker } = require('$config/config')

/* Services */
const TxStart = require('$services/txStart')
const TxCommit = require('$services/txCommit')
const TxAbort = require('$services/txAbort')

let zbc = new Zeebe.ZBClient(zeebe_broker, { logLevel: null })

var start = async () => {
    await zbc.createWorker(worker, 'dbtx-start', TxStart.handler, TxStart.config, TxStart.onConnectionError)
    await zbc.createWorker(worker, 'dbtx-commit', TxCommit.handler, TxCommit.config, TxCommit.onConnectionError)
    await zbc.createWorker(worker, 'dbtx-abort', TxAbort.handler, TxAbort.config, TxAbort.onConnectionError)
}

start()

module.exports = zbc