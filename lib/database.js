const log = require('simple-node-logger').createSimpleLogger()
const fs = require('fs')
const path = require('path')
const HashMap = require('hashmap')
const mongoose = require('mongoose')
const { primary_db, replica_set, models_path, ack_levels } = require('$config/config')

var DB
const DatabaseModels = new HashMap()

function loadModels() {
    var path_ = path.resolve(models_path);

    DatabaseModels.set('dl-logs', mongoose.model('dl-logs', require('$utils/logSchema')))

	fs.readdirSync(path_).forEach(async (file) => {
        try
        {
            let filename = path.basename(file, '.js')

            DatabaseModels.set(filename, mongoose.model(filename, require(path.resolve(path_, file))))
        }
        catch (err)
        {
            log.error('Error loading route', file, ' error,', err);
        }
	})
}
 
async function start(url) {
    try {
        DB = await mongoose.connect(url, { 
            useNewUrlParser: true,
            useUnifiedTopology: true,
            replicaSet: replica_set,
            connectTimeoutMS: 10000 })

        loadModels()

        log.info('Connection to database established')
    }
    catch(error) {
        log.error('[DATABASE] ', error.message)
    }
}

var startSessionTransaction = async () => {
    try {
        let sess = await DB.startSession({ defaultTransactionOptions: {
            readConcern: { level: ack_levels.read },
            writeConcern: { w: ack_levels.write },
            readPreference: 'primary'
        }})

        await sess.startTransaction()

        return sess
    }
    catch(error) {
        log.error('[DATABASE] ', error.message)
        throw error
    }
}

var dataOperations = {
    insert: async (coll, data, options) => {
        if(!Array.isArray(data))
            data = [data]
        
        return await DatabaseModels.get(coll).insertMany(data, options)
    },

    find: async (coll, query, select, options) => {
        return await DatabaseModels.get(coll).find(query, select || null, options)
    },

    update: async (coll, query, data, options) => await DatabaseModels.get(coll).updateMany(query, data, options),

    delete: async (coll, query, options) => await DatabaseModels.get(coll).deleteMany(query, options) 
}


start(primary_db)

module.exports = { DB, startSessionTransaction, dataOperations }