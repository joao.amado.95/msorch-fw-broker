const { dataOperations } = require('$lib/database')
const { getSession } = require('$lib/sessions')

const COLL = 'dl-logs'

var initiateTransaction = async function(workflowInstanceKey, dataTransactionKey,) {
    let tx = {
        wfKey: workflowInstanceKey,
        txKey: dataTransactionKey,
    }

    await dataOperations.insert(COLL, tx)
}

var insertTransactionOperation = async function(dataTransactionKey, op, payload) {
    let query = {
        txKey: dataTransactionKey,
    }

    delete payload.dataTransactionKey
    let txOp = JSON.stringify(Object.assign(payload, { type: op }))

    dataOperations.update(COLL, query, { $push: { transactions: txOp }}, { upsert: true })
}

var terminateTransaction = async function(dataTransactionKey, remove) {
    let query = {
        txKey: dataTransactionKey,
    }

    if(remove)
        await dataOperations.delete(COLL, query)
    else
        await dataOperations.update(COLL, query, { $set: { terminated: true }}, { upsert: false })

    return
}

var getUnfinishedTransactions = async () => await dataOperations.find(COLL, { terminated: false }, 'wfKey txKey')

var recoverTransaction = async function(dataTransactionKey) {
    let result = await dataOperations.find(COLL, { txKey: dataTransactionKey })
    if(result.length < 1 || result[0].terminated == true)
        return false
    
    for(let tx of result[0].transactions){
        let req = JSON.parse(tx)
        let { type, collection, operation } = req
        let { query, data, options } = operation
        
        options = Object.assign({}, options)
        options.session = getSession(dataTransactionKey)

        if(type == 'insert') {
            await dataOperations.insert(collection, data, options)
        }
        else if(type == 'update'){
            await dataOperations.update(collection, query, data, options)
        }
        else if(type == 'delete'){
            await dataOperations.delete(collection, query, options)
        }
        else {
            continue
        }
    }

    return true
}

module.exports = {
    initiateTransaction,
    insertTransactionOperation,
    terminateTransaction,
    getUnfinishedTransactions,
    recoverTransaction
}