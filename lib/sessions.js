const HashMap = require('hashmap')
const moment = require('moment')
const crypto = require('crypto')
const { secret, worker } = require('$config/config')

let Map = new HashMap()

var sessionKeyGen = (base) => {
    hmac = crypto.createHmac('sha256', secret);

    hmac.update(base)

    return worker + '_' + hmac.digest('base64')
}

var setSession = (key, session) => {
    let data = {
        lastAccess: moment(),
        session
    }

    Map.set(key, data)
}

var getSession = (key) => {
    let data = Map.get(key)
    
    //Update lastAccess
    if(data) {
        Map.delete(key)
        setSession(key, data.session)
    }

    return data? data.session : undefined
}

var removeSession = (key) => {
    let session = Map.get(key).session

    session.endSession()

    Map.remove(key)
}

dropMap = () => {
    for(let key of Map.keys())
        removeSession(key)
}

module.exports = {
    sessionKeyGen,
    setSession,
    getSession,
    removeSession,
    dropMap,
    Map
 }