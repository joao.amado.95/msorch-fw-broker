const log = require('simple-node-logger').createSimpleLogger()
const express = require('express')
const zbc = require('./worker')
const { dataOperations } = require('$lib/database')
const { getSession } = require('$lib/sessions')
const { insertTransactionOperation } = require('$lib/eventStore')
const errorHandler = require('$utils/errorHandler')
const { port } = require('$config/config')

var app = express()

app.use(express.json())


function setOptionsSession(txKey, options) {
    let session = getSession(txKey)
    let opts = null

    if(!session)
        throw new Error(`No session for ${txKey}`)
    else if(!session.inTransaction())
        throw new Error(`No transaction in session for ${txKey}`)

    if(options)
        opts = Object.assign({ session }, options)
    else
        opts = { session }

    return opts
}

app.post('/insert', async (req, res) => {
    let { dataTransactionKey, collection, operation } = req.body
    let { data, options } = operation
    let result, message = null
    
    let options_ = setOptionsSession(dataTransactionKey, options)

    try {
        result = await dataOperations.insert(collection, data, options_)
        
        insertTransactionOperation(dataTransactionKey, 'insert', req.body)
    }
    catch(error) {
        message = errorHandler({ error, dataTransactionKey }, '/insert')
    }

    return res.send({ result, message })
})

app.post('/find', async (req, res) => {
    let { dataTransactionKey, collection, operation } = req.body
    let { query, select, options } = operation
    let result, message = null

    let options_ = setOptionsSession(dataTransactionKey, options)
    
    try {
        result = await dataOperations.find(collection, query, select, options_)
        
        if(result.length < 1)
            message = 'No records'
        
        insertTransactionOperation(dataTransactionKey, 'find', req.body)
    }
    catch(error) {
        message = errorHandler({ error, dataTransactionKey }, '/find')
    }

    return res.send({ result, message })
})

app.post('/update', async (req, res) => {
    let { dataTransactionKey, collection, operation } = req.body
    let { query, data, options } = operation
    let result, message = null

    let options_ = setOptionsSession(dataTransactionKey, options)
    
    try {
        result = await dataOperations.update(collection,  query, data, options_)

        insertTransactionOperation(dataTransactionKey, 'update', req.body)
    }
    catch(error) {
        message = errorHandler({ error, dataTransactionKey }, '/update')
    }

    return res.send({ result, message })
})

app.post('/delete', async (req, res) => {
    let { dataTransactionKey, collection, operation } = req.body
    let { query, options } = operation
    let result, message = null

    let options_ = setOptionsSession(dataTransactionKey, options)
    
    try {
        result = await dataOperations.delete(collection, query, options_)

        insertTransactionOperation(dataTransactionKey, 'delete', req.body)
    }
    catch(error) {
        message = errorHandler({ error, dataTransactionKey }, '/delete')
    }

    return res.send({ result, message })
})

app.post('/abort', async (req, res) => {
    let { dataTransactionKey, message } = req.body

    try {
        await zbc.publishMessage({
            correlationKey: dataTransactionKey,
            name: 'abort-workflow',
            variables: { abortCause: message }
        })

        insertTransactionOperation(dataTransactionKey, 'abort', req.body)

        result = true
        message = `Transaction ${dataTransactionKey} aborted`
    }
    catch(error) {
        message = errorHandler({ error, dataTransactionKey }, '/abort')
        result = false
    }
    
    return res.send({ result, message })
})

var start = async () => {
    app.listen(port, (err) => {
        if (err) {
            return log.error('Not able to start server', err)
        }

        log.info('Interface is listening on port ', port)
    })
}

start()