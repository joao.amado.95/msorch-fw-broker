const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Order = new Schema({
    owner: String,
    type: String,
    amount: Number,
    price: Number,
    status: String
}, { collection: 'orders', timestamps: true });

module.exports = Order