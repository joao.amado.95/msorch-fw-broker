const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Account = new Schema({
    username: String,   
    name: String,
    surname: String,
    total: Number
}, { collection: 'accounts', timestamps: true });

module.exports = Account