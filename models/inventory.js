const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Inventory = new Schema({
    type: String,
    price: Number,
    amount: Number
}, { collection: 'inventory', timestamps: true });

module.exports = Inventory