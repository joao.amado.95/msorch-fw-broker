const log = require('simple-node-logger').createSimpleLogger()
log.setLevel(process.env.LOG_LEVEL)

const { sessionKeyGen, setSession } = require('$lib/sessions')
const { startSessionTransaction } = require('$lib/database')
const { initiateTransaction } = require('$lib/eventStore')

var config = {
    timeout: 1000,
    failWorkflowOnException: true,
}

var onConnectionError = err => { log.error('[SERVICE] ', err.message) }

var handler = async (job, complete) => {
    try {
        let dataTxKey = sessionKeyGen(job.workflowInstanceKey)

        //Start distributed transaction and store in state cache
        let tx = await startSessionTransaction()
        setSession(dataTxKey, tx)

        job.variables.dataTransactionKey = dataTxKey

        complete.success(job.variables)

        //Start logging data operations for workflow 'dataTxKey'
        initiateTransaction(job.workflowInstanceKey, dataTxKey)

        log.info(`Started data transaction ${job.variables.dataTransactionKey} for ${job.workflowInstanceKey}`)
    }
    catch(error) {
        log.error('Failing job [START TX] ', error)
        complete.failure(error.message, 0)
    }
}


module.exports = { config, handler, onConnectionError }