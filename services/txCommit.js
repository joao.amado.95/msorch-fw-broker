const log = require('simple-node-logger').createSimpleLogger()
log.setLevel(process.env.LOG_LEVEL)

const { getSession, removeSession } = require('$lib/sessions')
const { terminateTransaction } = require('$lib/eventStore')

var config = {
    maxActiveJobs: 32,
    timeout: 1000
}

var onConnectionError = err => { log.error('[SERVICE] ', err.message) }

var handler = async (job, complete) => {
    let dataTxKey = job.variables.dataTransactionKey

    if(!dataTxKey)
        return complete.failure("Field 'dataTransactionKey' is missing")

    try {
        let session = getSession(dataTxKey)

        await session.commitTransaction()
        
        removeSession(dataTxKey)
        
        complete.success()

        //Set termination flag for workflow 'dataTxKey'
        terminateTransaction(dataTxKey)
    }
    catch(error) {
        log.error('Failing job [COMMIT TX] ', error)
        complete.failure(error.message, 0)
    }

    log.info(`Commited data transaction ${dataTxKey} for ${job.workflowInstanceKey}`)
}


module.exports = { config, handler, onConnectionError }