/* Modules */
const log = require('simple-node-logger').createSimpleLogger()
const { startSessionTransaction } = require('$lib/database')
const { setSession } = require('$lib/sessions')
const { getUnfinishedTransactions, recoverTransaction } = require('$lib/eventStore')

async function transactionRecovery() {
    log.warn('Running transaction recovery...')

    let txs = await getUnfinishedTransactions()

    for(let tx of txs) {

        //Set new transaction in cache
        let session = await startSessionTransaction()
        setSession(tx.txKey, session)

        await recoverTransaction(tx.txKey)

        log.info(`Transaction ${tx.txKey} recovered`)
    }
}

var start = async () => {
    log.info('Initializing data layer...')
    
    try {
        await require('$lib/database')
        await require('$lib/worker')
        await require('$lib/interface')
        await require('$utils/stateCacheCleanUp')

        setTimeout(() => transactionRecovery(), 1000)
    }
    catch(error) {
        log.error('START', error)
        throw error
    }
}

var shutdown = async () => dropMap()

process.once('SIGINT', shutdown)
process.once('SIGTERM', shutdown)

start()