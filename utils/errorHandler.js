const log = require('simple-node-logger').createSimpleLogger()

//Receives axios error
module.exports = function(data, where) {
    log.warn(`Exception caught in ${where} for `, data.dataTransactionKey, ': ', data.error.message)
    return data.error.message
}