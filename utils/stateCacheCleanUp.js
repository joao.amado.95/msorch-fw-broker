const log = require('simple-node-logger').createSimpleLogger()
const moment = require('moment')
const { Map, removeSession } = require('$lib/sessions')
const { terminateTransaction } = require('$lib/eventStore')
const { session_timeout } = require('$config/config')

function cacheCleanUp() {
    log.info('Running cache clean up...')
    
    let entries = Map.entries()

    for(let pair of entries) {
        let now = moment().subtract(session_timeout, 'm')
        let last = moment(pair[1].lastAccess)

        if(now.isAfter(last)) {
            log.warn(`Removing stale session ${pair[0]} from cache...`)
            log.warn(`Now ${now} and lastAccess ${last}`)
            
            removeSession(pair[0])
            
            terminateTransaction(pair[0])
        }
    }
}

setInterval(cacheCleanUp, 4*30000)