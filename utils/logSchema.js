const { Schema } = require('mongoose')

const DLLogSchema = new Schema({
    wfKey: String,
    txKey: String,
    transactions: Array,
    terminated: {
        type: Boolean,
        default: false
    }
}, { collection: 'dl-logs' })

module.exports = DLLogSchema